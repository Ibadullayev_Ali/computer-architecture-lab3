# Computer-Architecture-lab3
Ибадуллаев Алибаба Эльбрус Оглы, гр. P33131\
`asm | risc | neum | hw | instr | binary | stream | mem | prob2`
### Язык программирования 
```text
program ::= string "\n" program

string ::= command
          | label
          | comment
          | section
          
command ::= zero_arg_command 
          | one_arg_command " " arg
          | two_arg_command " " arg " " arg 
          | three_arg_command " " arg " " arg " " arg 
          
comment ::= "; " name

sections ::= "section ." section_name

section_name ::= "data" | "text"
          
label ::=  name ": " 

name ::= "[a-z]"

zero_arg_command ::= "hlt" 

one_arg_command ::=  "jmp" | "read" | "write" | "word" | "inc" | "dec" 

two_arg_command ::= "mov" | "ld" | "st" 

three_arg_command ::= "add" | "sub" | "mod" | "bge" | "beq"

arg ::= name | addr | number

addr ::= [0; 2^16-1]

number ::= [-2^31-1; 2^31] 

```

Код выполняется последовательно. Операции:

add <arg1> <arg2> <arg3> -- прибавить ко второму аргументу первый и поместить обратно в регистр результат 

sub <arg1> <arg2> <arg3> -- вычесть из второго аргумента первый и поместить обратно в регистр результат

mod <arg1> <arg2> <arg3>-- получить остаток от деления первого аргумента на второй и поместить обратно в регистр результат 

beq <arg1> <arg2> <label> -- если значение в первом регистре равно значению во втором, перейти на аргумент-метку

bge  <arg1> <arg2> <label> -- если значение в первом регистре больше значения во втором, перейти на аргумент-метку

jmp <label> -- безусловный переход на аргумент-метку

mov <arg1> <arg2> -- скопировать значение из первого аргумента во второй

ld <arg1> <arg2> -- выгрузить значение в первый из памяти по первому аргументу (адресу)

st <arg1> <arg2> -- сохранение из регистра в память по второму аргументу (адресу)

write <arg> -- распечатать в поток вывода значение из аргумента

read <arg> -- прочитать в аргумент значение из потока ввода

word <number> -- объявление переменной

hlt -- завершить выполнение программы

inc <arg> -- инкремент значения в ячейке

dec <arg> -- декремент значения в ячейке


Поддерживаемые аргументы:

регистры: rdx, r1, r2, r3, r4, rnx, rcx

название объявленной метки

число в диапазоне [-2^32; 2^32 - 1]

Дополнительные конструкции:

; <any sequence not containing ';'> - комментарий

section .text - объявление секции кода

section .data - объявление секции данных

<label>: - метки для переходов / названия переменных

*Примечания:

Должна присутствовать метка start - точка входа в программу


## Система команд

Особенности процессора:

* Машинное слово - 32 битное знаковое
* Сначала размещаются данные, затем текст программы 
* Примечание если произойдет что данные достигли границ программы, то их продолжение будет после конца текста программы.
* Обращение к памяти происходит либо по увеличению `mem_counter` либо за счет помещения в него адреса перехода 
* Для команд условного перехода в `control unit` высчитывается условие и при соответствии происходит переход по адресу
* Для команд, где в качестве аргумента выступает регистр, на стадии выполнения команды подаются те регистры, которые будут участвовать в вычислении
* Регистров всего 7
* Прямая адресация
* В памяти находятся зарезервированные адреса под вывод вывод с внешнего устройства 



## Набор инструкций.

## Способ кодирования инструкций.



## Prob 2

### Fib value < 4 mln and all which is even I need find a sum


``` 
section .data

not_greater_than: 
    word 4000000
res: 
    word 0
    
    
section .text

start: 
    mov r1 1
    mov r2 1 

loop: 
    add rdx r1 r2
    mov r1 r2 
    mov rdx r1

is_greater_than:
; check if number is greater than 4 mln 
    ld not_greater_than rdx
    bge r1 rdx is_even
    jmp is_even

is_even: 
; check if number in rd is even 
    mod rnx rdx 2
    mov rdx 0 
    beq rnx rdx add
    jmp loop_last
sum_even:
    ld res rdx
    add rdx rdx r1
    st rdx res
loop_last: 
    dec rcx
    jmp loop
```
    